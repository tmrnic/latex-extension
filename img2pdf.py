#
# CODE - img2pdf.py
# @author :     TMR.Nic
# @date:        2024-04-14
# @function:    Using latex comand to transform images to pdf files.
# @version :    v1.0
# @contact:     www.navspace.tech
#

import os
import glob
import subprocess
from tqdm import tqdm

def get_images(directory):
    # Create an empty list to store image paths
    image_paths = []

    # Walk through the directory and its subdirectories
    for root, _, files in os.walk(directory):
        # Iterate over each file
        for file in files:
            # Check if the file extension is an image format
            if file.endswith((".jpg", ".jpeg", ".png", ".gif", ".bmp")):
                # Construct the absolute path to the image file
                image_path = os.path.join(root, file)
                # Append the image path to the list
                image_paths.append(image_path)

    # Return the list of image paths
    return image_paths



Image_dir = input("Input path: \n")
# Image_dir = os.path.abspath(".")
image_paths = get_images(Image_dir)

len = len(image_paths)
for i in tqdm(range(len)):
    Image_i = image_paths[i]                                # 图片文件路径
    output = Image_i.split('.')[0] + '.eps'                 # eps文件路径
    cmd = "bmeps.exe -c " + Image_i + " " + output          # 先转成eps
    subprocess.call(cmd,shell=True)
    
    #cmd = r"XXX\texlive\2022\bin\win32\epspdf.exe " + output       # 直接调用texlive中的程序
    cmd = "epspdf.exe " + output        # 将epspdf复制到同一个目录
    subprocess.call(cmd,shell=True)
    os.remove(output)                   # 如果有需要就不删除eps文件

 